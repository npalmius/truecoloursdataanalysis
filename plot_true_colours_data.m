function plot_true_colours_data(tc_data)
% Copyright (c) 2018, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Dec-2018

    questionaires = get_configuration('questionaires');
    questionaire_names = fieldnames(questionaires);
    
    t_min = inf;
    t_max = 0;
    
    subplots = numel(questionaire_names);
    
    h_plot = zeros(subplots, 1);
    q_min = zeros(subplots, 1);
    q_max = ones(subplots, 1);
    
    figure;
    
    if ~isempty(tc_data) && isstruct(tc_data) && ~isempty(fields(tc_data))
        raw_episodes = extract_raw_episodes(tc_data);
        
        for q = 1:subplots
            this_questionaire = questionaires.(questionaire_names{q});

            q_min(q) = this_questionaire.min;
            q_max(q) = this_questionaire.max;
            
            this_questionnaire_data = tc_data.(questionaire_names{q}).data;
            this_questionnaire_time = tc_data.(questionaire_names{q}).time;
            this_questionnaire_time = this_questionnaire_time(this_questionnaire_data > -1);
            this_questionnaire_data = this_questionnaire_data(this_questionnaire_data > -1);
            
            t_min = min(t_min, floor(min(this_questionnaire_time)));
            t_max = max(t_max, ceil(max(this_questionnaire_time)));
            
            h_plot(q) = subplot(subplots, 1, q);
            box on;
            hold on;
            
            threshold_line_properties = {'--' 'Color', ones(1, 3) * 0.35};

            plot([t_min, t_max], [q_min(q), q_min(q)], '-', threshold_line_properties{2:end});
            plot([t_min, t_max], [q_max(q), q_max(q)], '-', threshold_line_properties{2:end});

            if strcmp(questionaire_names{q}, 'ALTMAN') == 1
                plot([t_min, t_max], [6, 6], threshold_line_properties{:});
                plot([t_min, t_max], [10, 10], threshold_line_properties{:});
            elseif strcmp(questionaire_names{q}, 'QIDS') == 1
                plot([t_min, t_max], [6, 6], ':', threshold_line_properties{2:end});
                plot([t_min, t_max], [11, 11], threshold_line_properties{:});
                plot([t_min, t_max], [16, 16], threshold_line_properties{:});
                plot([t_min, t_max], [21, 21], threshold_line_properties{:});
            end
            
            stairs(raw_episodes.time, raw_episodes.(questionaire_names{q}), 'Color', this_questionaire.line_colour, 'LineWidth', 1);
            scatter(this_questionnaire_time, this_questionnaire_data, 'x', 'MarkerEdgeColor', this_questionaire.line_colour);

            q_range = q_max(q) - q_min(q);
            
            q_min(q) = q_min(q) - q_range * 0.07;
            q_max(q) = q_max(q) + q_range * 0.07;
            
            ylim([q_min(q) q_max(q)]);
            
            ylabel(this_questionaire.display_name, 'FontSize', 14);
            
            if q == 1
                title('True Colours', 'FontSize', 16);
            end
            
            if q < subplots
                set(gca, 'xticklabel', [], 'FontSize', 12);
            else
                [xticks, xticks_str] = get_xticks(t_min, t_max);
                
                set(gca, 'xticklabel', xticks_str, 'FontSize', 12);
                
                for h = h_plot'
                    set(h, 'xtick', xticks);
                    xlim(h, [t_min, t_max]);
                end
            end
            
            p = get(gca, 'Position');
            p_diff = p(4) * 0.2;
            p(4) = p(4) + p_diff;
            p(2) = p(2) - ((3 - (q - 1)) * (p_diff / 3));
            set(gca, 'Position', p);
        end
        subplot(h_plot(1));
    end
end
