function raw_episodes = extract_raw_episodes(tc_data)
% Copyright (c) 2018, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Dec-2018

    t_min = inf;
    t_max = 0;

    raw_episodes = struct();
    
    if ~isempty(tc_data) && isstruct(tc_data) && ~isempty(fields(tc_data))
        questionaires = fieldnames(tc_data)';
        
        for this_questionaire = questionaires
            if isfield(tc_data, this_questionaire{:}) && isfield(tc_data.(this_questionaire{:}), 'data')
                this_questionnaire_time = tc_data.(this_questionaire{:}).time;
                if ~isempty(this_questionnaire_time)
                    t_min = min(t_min, floor(min(this_questionnaire_time)));
                    t_max = max(t_max, ceil(max(this_questionnaire_time)));
                end
            end
        end

        raw_episodes.time = (t_min:(t_max - 1))';
        raw_episodes.manic = false(t_max - t_min, 1);
        raw_episodes.depressed = false(t_max - t_min, 1);
        raw_episodes.manic_severity = NaN(t_max - t_min, 1);
        raw_episodes.depressed_severity = NaN(t_max - t_min, 1);
        raw_episodes.epochs = [];

        l = 1;

        for this_questionaire = questionaires
            if strcmp(this_questionaire{:}, 'ALTMAN') == 1
                episode_name = 'manic';
                episode_threshold = 6;
                episode_min_length = 7;
                severity_thresholds = [0 NaN 6 10];
            elseif strcmp(this_questionaire{:}, 'QIDS') == 1
                episode_name = 'depressed';
                episode_threshold = 11;
                episode_min_length = 14;
                severity_thresholds = [0 6 11 16];
            else
                episode_name = NaN;
                episode_threshold = inf;
                episode_min_length = inf;
                severity_thresholds = [0 inf inf inf];
            end
            
            raw_episodes.(this_questionaire{:}) = NaN(t_max - t_min, 1);

            severity_name = [episode_name '_severity'];

            if isfield(tc_data, this_questionaire{:}) && isfield(tc_data.(this_questionaire{:}), 'data')
                data_valid = tc_data.(this_questionaire{:}).data > -1;

                this_questionnaire_data = tc_data.(this_questionaire{:}).data(data_valid);
                this_questionnaire_time = tc_data.(this_questionaire{:}).time(data_valid);
                this_questionnaire_time_diff = [inf; diff(floor(this_questionnaire_time))];

                % Start at the last questionnaire response and work
                % backwards to the first.
                i = length(this_questionnaire_data);
                while i >= 1
                    if this_questionnaire_data(i) < episode_threshold
                        % For questionnaire scores below the threshold,
                        % just mark the severity for the period
                        % corresponding to the questionnaire.
                        questionnaire_period_start = ceil(this_questionnaire_time(i)) - min(max(this_questionnaire_time_diff(i) - 1, 0), 6);
                        questionnaire_period = max(1, (questionnaire_period_start - t_min)):(ceil(this_questionnaire_time(i)) - t_min);

                        if ~isnan(episode_name)
                            % Get severity according to questionnaire score,
                            % and save it in the corresponding period.
                            severity = find(this_questionnaire_data(i) >= severity_thresholds, 1, 'last') - 1;
                            raw_episodes.(severity_name)(questionnaire_period) = severity;
                        end

                        raw_episodes.(this_questionaire{:})(questionnaire_period) = this_questionnaire_data(i);

                        i = i - 1;
                    else % (this_questionnaire_data(i) >= episode_threshold)
                        % This questionnaire is above the threshold, so
                        % marks the end of an episode.
                        episode_end = ceil(this_questionnaire_time(i));

                        while this_questionnaire_data(i) >= episode_threshold
                            % Determine the period corresponding to this
                            % questionnaire response, and make this the new
                            % start of the episode.
                            episode_start = ceil(this_questionnaire_time(i)) - min(max(this_questionnaire_time_diff(i) - 1, 0), 6);
                            questionnaire_period = max(1, (episode_start - t_min)):(ceil(this_questionnaire_time(i)) - t_min);

                            % Get severity according to questionnaire
                            % score, and save it in the corresponding
                            % period.
                            severity = find(this_questionnaire_data(i) >= severity_thresholds, 1, 'last') - 1;
                            raw_episodes.(severity_name)(questionnaire_period) = severity;

                            raw_episodes.(this_questionaire{:})(questionnaire_period) = this_questionnaire_data(i);

                            i = i - 1;

                            if this_questionnaire_time_diff(i + 1) >= 28
                                break;
                            end
                        end

                        if (episode_end - episode_start + 1) >= episode_min_length
                            raw_episodes.(episode_name)(max(1, (episode_start - t_min)):(episode_end - t_min)) = true;
                            
                            if l == 1
                                raw_episodes.epochs = struct('type', '', 'start', 0, 'end', 0);
                            end

                            raw_episodes.epochs(l).type = episode_name;
                            raw_episodes.epochs(l).start = episode_start;
                            raw_episodes.epochs(l).end = episode_end;

                            l = l + 1;
                        end
                    end
                end
            end
        end
        
        if l > 1
            raw_episodes.epochs = fliplr(raw_episodes.epochs);
        end
    end
end
