function [tc_data, raw_episodes, episodes_for_analysis, episode_stats] = load_and_plot_true_colours_data_file_amoss(participant_id)
% Copyright (c) 2018, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Dec-2018

    this_path = fileparts(mfilename('fullpath'));
    
    data_file_path = sprintf('%s/Data/%s.csv', this_path, participant_id);
    
    tc_data = load_true_colours_data_file_amoss(data_file_path);
    raw_episodes = extract_raw_episodes(tc_data);
    
    plot_true_colours_data(tc_data);
    title('True Colours Data', 'FontSize', 14);
    save_figure(sprintf('%s_data', participant_id));
    close;
    
    plot_true_colours_raw_episodes(tc_data);
    title('True Colours Data with Raw Episodes', 'FontSize', 14);
    save_figure(sprintf('%s_episodes', participant_id));
    close;
    
    episodes_for_analysis = extract_episodes_for_analysis_1(tc_data);
    plot_true_colours_episodes_for_analysis(tc_data, episodes_for_analysis);
    title('True Colours Data with Episodes for Analysis (1)', 'FontSize', 14);
    save_figure(sprintf('%s_episodes_for_analysis_1', participant_id));
    close;
    
    episodes_for_analysis = extract_episodes_for_analysis_2(tc_data);
    plot_true_colours_episodes_for_analysis(tc_data, episodes_for_analysis);
    title('True Colours Data with Episodes for Analysis (2)', 'FontSize', 14);
    save_figure(sprintf('%s_episodes_for_analysis_2', participant_id));
    close;
    
    episode_stats = get_episode_stats(raw_episodes);
    
    fprintf('Raw episodes extracted:\n')
    
    total_episodes = 0;
    total = 0;
    
    for episode_cell = fieldnames(episode_stats)'
        episode_display_name = episode_cell{:};
        episode_display_name(1) = upper(episode_display_name(1));
        if ~strcmp(episode_cell{:}, 'euthymic')
            total_episodes = total_episodes + episode_count;
            episode_display_name = sprintf('%s episodes', episode_display_name);
        else
            episode_display_name = sprintf('%s periods', episode_display_name);
        end
        episode_count = episode_stats.(episode_cell{:}).episode_count;
        fprintf('  %s: %i\n', episode_display_name, episode_count);
        total = total + episode_count;
    end
    
    fprintf('  Total episodes: %i\n', total_episodes);
    fprintf('  Total: %i\n', total);
    
    fprintf('Episodes extracted for analysis:\n')
    
    total_episodes = 0;
    total = 0;
    
    for episode_cell = fieldnames(episode_stats)'
        episode_display_name = episode_cell{:};
        episode_display_name(1) = upper(episode_display_name(1));
        if ~strcmp(episode_cell{:}, 'euthymic')
            total_episodes = total_episodes + episode_count;
            episode_display_name = sprintf('%s episodes', episode_display_name);
        else
            episode_display_name = sprintf('%s periods', episode_display_name);
        end
        episode_count = episode_stats.(episode_cell{:}).episode_count;
        fprintf('  %s: %i\n', episode_display_name, episode_count);
        total = total + episode_count;
    end
    
    fprintf('  Total episodes: %i\n', total_episodes);
    fprintf('  Total: %i\n', total);
end