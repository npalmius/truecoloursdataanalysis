# True Colours Analysis Scripts
This repository contains a selection of MATLAB scripts for processing True Colours questionnaire data.

## Loading data
True colours data can be loaded using the following command:

    tc_data = load_and_plot_true_colours_data_file_amoss(filename);

For example, if data files are placed in the `\Data` directory then data can be loaded as:

    tc_data = load_and_plot_true_colours_data_file_amoss('Data\data_file.csv');

but files can also be placed anywhere and a full path provided.
 
This loads the questionnaire data into a structure with a field for each questionnaire in the data file, for example:

* `ALTMAN`: data relating to the Altman mania questionnaire.
* `EQ_5D`: data relating to the EQ-5D quality of life questionnaire.
* `GAD_7`: data relating to the GAD-7 anxiety questionnaire.
* `QIDS`: data relating to the QIDS depression questionnaire.

The data for each questionnaire are stored as a structure containing the following fields:

* `time`: the date/timestamp of the data values.
* `message`: the string data from the individual question responses field in the raw data file.
* `questions`: the individual question responses, as a vector of numeric values.
* `data`: the overall questionnaire score, taken directly from the raw data file. Missing responses have value -1.

The loaded data can be plotted using the functions described in the [Plotting data](#markdown-header-plotting-data) section below to produce a plot similar to the following, with the four questionnaires in the data shown on each row (not available online).
Each questionnaire response is represented by a cross, and the line indicates the days for which each response is considered to apply for (detailed in the next section).  

![](doc/img/14005_data.png)

## Extracting episodes
Episodes can be extracted from the loaded data in several ways.

### Raw episodes
The simplest method to extract 'raw' episodes is the following command:

    raw_episodes = extract_raw_episodes(tc_data);

This method simply thresholds the total Altman and QIDS questionnaire scores to find episodes of mania and depression respectively.

The following rules are applied to extract the episodes:

* **Mania-specific rules:**
	* Altman questionnaire scores of 6 or more.
	* Total episode length lasting for 7 days or more.     
* **Depression-specific rules:**
	* QIDS questionnaire scores of 11 or more.
	* Total episode length lasting for 14 days or more.
* **Common rules for both types of episode**
	* A questionnaire score is defined as applying from the later of:
		* 6 days before the questionnaire response; or
		* the day after the previous questionnaire response.
	* A questionnaire score applies until and including the day of the questionnaire response.
	* Hence a questionnaire score applies for a maximum of 7 days.
	* Consecutive above-threshold questionnaire responses separated by less than 28 days are counted as a single episode.
	* If a below-threshold response on a given day is followed by an above-threshold response on the same day, then that day is counted as the start of an episode (note that the questionnaire response only applies to that day).
	* An episode with a single below-threshold point within it is counted as two separate episodes.
	* Mixed episodes are not considered, but are included as separate manic and depressed episodes.

The output of the `extract_raw_episodes` function is a structure containing the following fields:

* `time`: the date of the data values, with one record per day throughout the available True Colours data.
* `manic`: boolean vector specifying whether the individual is considered to be in a manic episode on any given day.
* `depressed`: boolean vector specifying whether the individual is considered to be in a depressed episode on any given day.
* `manic_severity`: integer-valued vector specifying the severity of manic symptoms on any given day, with the following values:
	* 0: No manic symptoms (Altman < 6)
	* 2: Moderate manic symptoms (6 &le; Altman < 10)
	* 3: Severe manic symptoms (Altman &ge; 10)
* `depressed_severity`: integer-valued vector specifying the severity of depressive symptoms on any given day, with the following values:
	* 0: No depressive symptoms (QIDS < 6)
	* 1: Mild depressive symptoms (6 &le; QIDS < 11)
	* 2: Moderate depressive symptoms (11 &le; QIDS < 16)
	* 3: Severe depressive symptoms (QIDS &ge; 16)
* `ALTMAN` / `QIDS` / etc.: a separate field for each questionnaire specifying the nearest response (if any) applied to any given day (days with no applicable response within the next 7 days are set to `NaN`).
* `epochs`: a structure array with a record for each episode identified, with the following fields:
	* `type`: the type of episode (manic/depressed)
	* `start`: the first day of the episode.
	* `end`: the last day of the episode. 

Severity and questionnaire values are applied for the validity of each questionnaire response, i.e. from the later of 6 days before the questionnaire response; or the day after the previous questionnaire response, until the day of the questionnaire response.

Two samples of extracted raw episodes can be seen below (not available online).
The first row shows the Altman questionnaire responses and extracted manic episodes shaded.
The second row shows the GAD-7 questionnaire responses.
The third row shows the QIDS questionnaire responses and extracted depressed episodes shaded.
The fourth row shows the EQ-5D questionnaire responses.
The second last row shows the severity calculated for each day from the nearest questionnaire response (manic severity is shown as positive; depressive severity is shown as negative).
The last row shows the extracted episodes, with manic episodes shown as positive and depressive episodes shown as negative.

![](doc/img/14005_episodes.png)
![](doc/img/14010_episodes.png)

### Episodes for analysis

Episodes for analysis can be extracted using one of the following two commands:

    episodes_for_analysis = extract_episodes_for_analysis_1(tc_data);
	episodes_for_analysis = extract_episodes_for_analysis_2(tc_data);

Both of these commands post-process the raw extracted episodes to provide a list of periods where an episode can be identified with high certainty.
This is useful for comparing characteristics of individuals in episodes while ignoring transition periods between episodes.

The output of both of these functions is a structure containing the following fields:

* `time`: the date of the data values, with one record per day throughout the available True Colours data.
* `manic`: boolean vector specifying whether the individual is considered to be in a manic episode on any given day.
* `depressed`: boolean vector specifying whether the individual is considered to be in a depressed episode on any given day.
* `euthymic`: boolean vector specifying whether the individual is considered to be in a euthymic state on any given day.
* `mixed`: boolean vector specifying whether the individual is considered to be in a mixed depressed/manic episode on any given day (only provided by the `extract_episodes_for_analysis_2` function).
* `epochs`: a structure array with a record for each episode identified, with the following fields:
	* `type`: the type of episode (manic/depressed)
	* `start`: the first day of the episode.
	* `end`: the last day of the episode.
* `epochs_split`: a structure array with a record for each episode identified, split into two-week consecutive periods, with the following fields (only relevant for the `extract_episodes_for_analysis_2` function):
	* `type`: the type of the split episode (manic/depressed/euthymic/mixed)
	* `start`: the first day of the episode.
	* `end`: the last day of the episode.

#### Episodes for analysis version 1 script

the first version of the episodes for analysis can be extracted using the following command:

    episodes_for_analysis = extract_episodes_for_analysis_1(tc_data);

This command places the following criteria on the extracted episodes:

* Only extracts non-overlapping manic and depressive episodes (mixed periods are ignored).
* Excludes the first week of an episode or euthymic period.
* The minimum length of an episode is 28 days (after exclusion of the first week).

Samples of the extracted features from the version 1 script can be seen below (not available online) where the episodes have been shaded in the last row.

![](doc/img/14005_episodes_for_analysis_1.png)
![](doc/img/14010_episodes_for_analysis_1.png)

#### Episodes for analysis version 2 script

the first version of the episodes for analysis can be extracted using the following command:

    episodes_for_analysis = extract_episodes_for_analysis_2(tc_data);

This command places the following criteria on the extracted episodes:

* Extracts non-overlapping manic, depressive and mixed episodes.
* Excludes the first week of an episode or euthymic period.
* The minimum length of an episode is 14 days (after exclusion of the first week).
* Also return each episode split into consecutive two-week periods in the `epochs_split` field of the output structure.

Samples of the extracted features from the version 2 script can be seen below (not available online) where the episodes have been shaded in the last row.

![](doc/img/14005_episodes_for_analysis_2.png)
![](doc/img/14010_episodes_for_analysis_2.png)

## Extracting properties of questionnaires and raw episodes

Basic properties of the questionnaire responses and raw episodes can be extracted using the following command:

    stats = get_stats(tc_data)

The output of this functions is a structure containing the following fields:

* `manic`/ `depressed`: properties of any extracted manic or depressed raw episodes respectively, each of which is a structure with the following fields:
	* `episode_count`: the number of episodes extracted.
	* `episode_length`: a vector containing the length (in days) of each episode extracted.
	* `pct_above_threshold`: the percentage of days that have a questionnaire response within the next 7 days that are above the thresholds for moderate mania / depression (defined as Altman &ge; 6 and QIDS &ge; 11 respectively).
* `ALTMAN` / `QIDS` / etc.: a separate field for each questionnaire specifying the properties of the responses to that questionnaire, each of which is a structure with the following fields:
	* `pct_missing`: the percentage of days that have no valid questionnaire response within the next 7 days.
    * `mean`: the mean value of questionnaire responses applied to each day where a valid questionnaire response is available within the next 7 days.
    * `median`: the median value of questionnaire responses applied to each day where a valid questionnaire response is available within the next 7 days.

## Extracting episode properties

Basic properties of the extracted episodes (either raw episodes or episodes for analysis) can be obtained using the following command:

    episode_stats = get_episode_stats(raw_episodes)
    episode_stats = get_episode_stats(episodes_for_analysis)

The output of this functions is a structure containing the following fields:

* `manic`: properties of any extracted manic episodes.
* `depressed`: properties of any extracted depressed episodes.
* `euthymic`: properties of any extracted euthymic periods (if relevant).
* `mixed`: properties of any extracted mixed episodes (if relevant).

All of the fields in the output structure are themselves structures with the following fields:

* `episode_count`: the number of episodes extracted.
* `episode_length`: a vector containing the length (in days) of each episode extracted.

## Plotting data
The loaded data and extracted episodes can be extracted using the following functions (used to generate the figures shown above).

The raw data, without any extracted episodes, can be plotted using the following command:

    plot_true_colours_data(tc_data)

The raw episodes, can be plotted using the following command:

    plot_true_colours_raw_episodes(tc_data)

The episodes for analysis, can be plotted using the following command:

    plot_true_colours_episodes_for_analysis(tc_data, episodes_for_analysis)

## Demo function

If data files are placed in the `\Data` directory, a demo function is available to extract the episodes from the data and plot the output graphs.

If a data file named `sample_data.csv` is placed in the `\Data` directory, then the demo function is run using the following command:

    load_and_plot_true_colours_data_file_amoss('sample_data')

This will load the data and produce a set of figures in the `\Figures` directory.
