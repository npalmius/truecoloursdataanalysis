function value = get_configuration(configuration_name)
% Copyright (c) 2018, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Dec-2018

    value = [];
    
    switch lower(configuration_name)
        case 'episode_types'
            clr = lines(7);
            
            value = struct();
            value.euthymic  = struct('id', 0, 'display_name', 'Euthymic',  'display_name_short', 'Euth.', 'line_colour', clr(3, :), 'episode_colour', min(1, clr(3, :) + 0.6), 'epoch_colour', min(1, clr(3, :) + 0.2));
            value.depressed = struct('id', 1, 'display_name', 'Depressed', 'display_name_short', 'Dep.',  'line_colour', clr(7, :), 'episode_colour', min(1, clr(7, :) + 0.6), 'epoch_colour', min(1, clr(7, :) + 0.2));
            value.manic     = struct('id', 2, 'display_name', 'Manic',     'display_name_short', 'Manic', 'line_colour', clr(4, :), 'episode_colour', min(1, clr(4, :) + 0.5), 'epoch_colour', min(1, clr(4, :) + 0.2));
            value.mixed     = struct('id', 3, 'display_name', 'Mixed',     'display_name_short', 'Mixed', 'line_colour', clr(1, :), 'episode_colour', min(1, clr(1, :) + 0.6), 'epoch_colour', min(1, clr(1, :) + 0.2));
        case 'questionaires'
            clr = lines(7);
            
            value = struct();
            value.ALTMAN = struct('id', 0, 'display_name', 'ALTMAN', 'line_colour', clr(4, :), 'min', 0, 'max', 20);
            value.GAD_7  = struct('id', 1, 'display_name', 'GAD-7',  'line_colour', clr(1, :), 'min', 0, 'max', 21);
            value.QIDS   = struct('id', 2, 'display_name', 'QIDS',   'line_colour', clr(7, :), 'min', 0, 'max', 27);
            value.EQ_5D  = struct('id', 3, 'display_name', 'EQ-5D',  'line_colour', clr(2, :), 'min', 0, 'max', 100);
    end
end
