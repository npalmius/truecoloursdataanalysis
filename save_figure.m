function save_figure(filename)
% Copyright (c) 2018, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Dec-2018

    this_path = fileparts(mfilename('fullpath'));

    output_path = sprintf('%s/Figures/', this_path);
    
    save_eps = true;
    save_fig = true;
    save_png = true;
    save_pdf = true;
    
    haxes = get(gcf, 'children');
    
    if exist(fileparts([output_path filename]), 'dir') ~= 7
        mkdir(fileparts([output_path filename]));
    end
    
    axes_idx = ismember([{} get(haxes, 'type')], 'axes');
    real_axis_count = sum(axes_idx);
    
    current_title = cell(real_axis_count, 1);
    remove_title = false;
    
    if (save_eps || save_pdf) && real_axis_count >= 1
        t = get(haxes(axes_idx), 'title');
        if real_axis_count == 1
            current_title{1} = get(t, 'String');
            remove_title = true;
        else
            for i = 1:real_axis_count
                current_title{i} = get(t{i}, 'String');
            end
            remove_title = true;
            for i = 1:(real_axis_count - 1)
                this_text = current_title{i};
                if iscell(this_text) && (numel(this_text) > 1 || (~isempty(this_text{1}) && ~strcmp(this_text{1}, char(160))))
                    remove_title = false;
                elseif ~isempty(current_title{i}) && ~strcmp(current_title{i}, char(160))
                    remove_title = false;
                end
            end
        end
    end
    
    i = 1;
    while i > 0
        try
            if save_png
                saveas(gcf, [output_path filename], 'png');
            end
            if save_fig
                saveas(gcf, [output_path filename], 'fig');
            end
            if (save_eps || save_pdf)
                if remove_title && real_axis_count == 1
                    title(haxes(axes_idx), '');
                elseif remove_title && real_axis_count > 1
                    for ax = find(axes_idx)'
                        title(haxes(ax), '');
                    end
                end
            end
            if save_eps
                saveas(gcf, [output_path filename], 'epsc');
            end
            if save_pdf
                saveas(gcf, [output_path filename], 'pdf');
            end
            i = 0;
        catch err
            warning(err.message);
            i = i + 1;
            if i >= 100
                rethrow(err);
            end
            pause(0.1);
        end
        if (save_eps || save_pdf) && remove_title && real_axis_count >= 1
            j = 1;
            for ax = find(axes_idx)'
                title(haxes(ax), current_title{j});
                j = j + 1;
            end
        end
    end
end
