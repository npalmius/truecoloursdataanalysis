function episodes_for_analysis = extract_episodes_for_analysis_2(tc_data)
% Copyright (c) 2018, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 18-Dec-2018

% Criteria:
%   * Return non-overlapping manic and depressive episodes and mixed
%     episodes
%   * Exclude first week of episode or normal period
%   * Minimum epsode length 14 days (after exclusion of first week)
%   * Return all non-overlapping 2-week periods within selected episodes
%
% M: ������|___________|�������������|________________________  � = Manic
% D: �������|__________|������|______________|����������������  _ = Dep.
%      ____    ________   ____   ____   _____   _____________ 
%     | M  |  |   D    | | M  | | Mx | |  D  | |   NORMAL    |
%     |__|    |__|__|__| |__|   |__|   |__|__| |__|__|__|__|  

    raw_episodes = extract_raw_episodes(tc_data);
    
    episodes_for_analysis = struct();

    if ~isempty(raw_episodes) && isstruct(raw_episodes) && ~isempty(fields(raw_episodes))
        episodes_for_analysis.time = raw_episodes.time;
        episodes_for_analysis.manic = raw_episodes.manic & ~raw_episodes.depressed;
        episodes_for_analysis.depressed = raw_episodes.depressed & ~raw_episodes.manic;
        episodes_for_analysis.euthymic = ~raw_episodes.depressed & ~raw_episodes.manic & ~isnan(raw_episodes.depressed_severity) & ~isnan(raw_episodes.manic_severity);
        episodes_for_analysis.mixed = raw_episodes.depressed & raw_episodes.manic;
        episodes_for_analysis.epochs = [];
        episodes_for_analysis.epochs_split = [];

        any_episode = raw_episodes.manic | raw_episodes.depressed | episodes_for_analysis.euthymic;
        any_changes = diff(any_episode);

        if sum(abs(any_changes)) > 0
            any_changes_idx = find(abs(any_changes) > 0);

            if any_changes(any_changes_idx(1)) == 1
                any_changes_idx = any_changes_idx(2:end);
            end

            for i = 1:(length(any_changes_idx) - 1)
                if (any_changes_idx(i + 1) - any_changes_idx(i)) < 21
                    if episodes_for_analysis.euthymic(any_changes_idx(i)) && episodes_for_analysis.euthymic(any_changes_idx(i + 1) + 1)
                        episodes_for_analysis.euthymic((any_changes_idx(i) + 1):any_changes_idx(i + 1)) = true;
                    end
                end
            end
        end

        episodes_for_analysis.euthymic = episodes_for_analysis.euthymic;

        l = 1;
        l_split = 1;

        for mood_cell = {'manic', 'depressed', 'euthymic', 'mixed'}
            for i = find(diff([0; episodes_for_analysis.(mood_cell{:})]) > 0)'
                episodes_for_analysis.(mood_cell{:})(i:(i + 6)) = false;
            end

            episode_changes = diff([0; episodes_for_analysis.(mood_cell{:})]);
            episode_changes_idx = find(abs(episode_changes) > 0);

            i = 1;

            episode_min = episodes_for_analysis.time(1);
            episode_min_idx = 1;

            while i <= length(episode_changes_idx)
                if episode_changes(episode_changes_idx(i)) > 0
                    episode_min = episodes_for_analysis.time(episode_changes_idx(i));
                    episode_min_idx = episode_changes_idx(i);

                    if i == length(episode_changes_idx)
                        episode_max = episodes_for_analysis.time(end) + 1;
                        episode_max_idx = length(episodes_for_analysis.(mood_cell{:}));
                    end
                else
                    episode_max = episodes_for_analysis.time(episode_changes_idx(i));
                    episode_max_idx = episode_changes_idx(i) - 1;
                end
                if (episode_changes(episode_changes_idx(i)) < 0) || (i == length(episode_changes_idx))
                    if (episode_max - episode_min) < 14
                        episodes_for_analysis.(mood_cell{:})(episode_min_idx:episode_max_idx) = false;
                    else
                        if l == 1
                            episodes_for_analysis.epochs = struct('type', '', 'start', 0, 'end', 0);
                        end

                        episodes_for_analysis.epochs(l).type = mood_cell{:};
                        episodes_for_analysis.epochs(l).start = episode_min;
                        episodes_for_analysis.epochs(l).end = episode_max - 1;
                        
                        l = l + 1;
                        
                        if l_split == 1
                            episodes_for_analysis.epochs_split = struct('type', '', 'start', 0, 'end', 0);
                        end
                        
                        this_episode_min = episode_min;

                        while this_episode_min + 13 < episode_max
                            episodes_for_analysis.epochs_split(l_split).type = mood_cell{:};
                            episodes_for_analysis.epochs_split(l_split).start = this_episode_min;
                            episodes_for_analysis.epochs_split(l_split).end = this_episode_min + 13;

                            this_episode_min = this_episode_min + 14;

                            l_split = l_split + 1;
                        end
                    end
                end

                i = i + 1;
            end
        end
    end
end
